## Beispiele

## Docker
### Vorbereitung
- Docker Desktop installieren: [docker for windows](https://docs.docker.com/docker-for-windows/install/)

### Docker als Server
- Navigiere zum Ordner "**docker als server**" unter "**Beispiele**"
- Öffne im Ordner eine Konsole (CMD/Powershell/sonstige)
- baue ein docker image mit `docker build . -t myserverimage:v1.2`
- - der . (Punkt) verweist auf den Pfad, für den das Image gebaut werden muss
- - die Dockerfile wird dann automatisch gefunden
- - -t taggt das image mit name und tag(version)
- starte container mit diesem image mit `docker run -p 9001:9000 --name mylocalserver myserverimage:v1.2`
- - image-name und tag entsprechen dem vorher gebauten image
- - -p leitet die ports weiter, auf die unser Server hört (ist im Server so einprogrammiert)
- **Fertig**
- - nun ist der Server ansprechbar über localhost:9001
- - ein möglicher Aufruf wäre "localhost:9001/api/ships"
- - dieser lässt sich über konsole ausführen mit `curl localhost:9001/api/ships`

### Docker als Executable
- Navigiere zum Ordner "**docker als executable**" unter "**Beispiele**"
- Öffne im Ordner eine Konsole
- starte container mit `docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate -i https://petstore.swagger.io/v2/swagger.json -l go -o /local/out/go`
- **Fertig**
- - das Program hat aus Konfiguration in einer .json Datei (liegt im Web) einen Client in Sprache "Go" erstellt

## Kubernetes
### Vorbereitung
- "**all you need is docker**"
- - der kubernetes cluster läuft über *kind* (Kubernets in docker) in einem docker-image

### Beispiel
- Navigiere zum Ordner "**kind cluster**" unter "**Beispiele**"
- Öffne im Ordner eine Konsole (CMD/Powershell/sonstige)
- baue das docker image mit dem integrierten cluster: `docker build . -t kindlf11test`
- starte dieses image mit `docker run --rm --name lf11demo -v /var/run/docker.sock:/var/run/docker.sock -it --mount type=bind,source=$(pwd)/mounted,target=/root/mntd kindlf11test`
- **Fertig**
- - man ist nun "im" erstellten container, und kann dort den kubernetes cluster konfigurieren.
- - - z.B. kann man den Status der Pods abfragen:
- - - `kubectl get pods` (listet alle "pods" auf)
- - der Ordner mounted ist an den Container gebunden - d.h. Änderungen im Host-System wirken sich auf den Container aus (und vice versa).
- - - Damit kann man im host-system komfortabel die kubernetes konfiguration (yaml files) editieren, und im container dann nur anwenden mit "kubectl apply"